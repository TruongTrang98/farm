const ObjectId = require('mongoose').Types.ObjectId;
const StatusCode = require('../constants/StatusCode');

module.exports = {
  check(res, id) {
    if (!ObjectId.isValid(id)) {
      return res.status(StatusCode.INVALID_PARAMS).json({
        "massage": "Id không hợp lệ"
      })
    }

    return true;
  }
}