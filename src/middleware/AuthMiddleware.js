const jwtHelper = require('../helpers/jwt');
const accessTokenSecret = process.env.SECRET_KEY || "access-token-secret-kadon-farm@";
const StatusCode = require('../constants/StatusCode');

let isAuth = async (req, res, next) => {
  const tokenFromClient = req.body.token || req.query.token || req.headers["x-access-token"];
  if (tokenFromClient) {
    try {
      const decoded = await jwtHelper.verifyToken(tokenFromClient, accessTokenSecret);
      req.jwtDecoded = decoded;
      next();
    } catch (error) {
      return res.status(StatusCode.INVALID_TOKEN).json({
        message: 'Unauthorized.',
      });
    }
  } else {
    return res.status(StatusCode.NO_TOKEN_PROVIDED).json({
      message: 'No token provided.',
    });
  }
}
module.exports = {
  isAuth,
};