const nodemailer = require('nodemailer');
const UserMailModel = require('../models/UserMailModel');
const mailConfig = require('../configs/sendMailData');

module.exports = {
  sendMail = async (subject, data, htmlFile) => {
    const transporter = nodemailer.createTransport({
      host: mailConfig.host,
      port: mailConfig.port,
      secure: false,
      auth: {
        user: mailConfig.user,
        pass: mailConfig.pass
      }
    })

    const receivers = await UserMailModel.find().map(i => i.email)

    // Map data to html file by read html File and put data to it
    // const htmlContent = 
    const options = {
      from: mailConfig.user,
      to: receivers,
      subject,
      html: htmlContent
    }

    return await transporter.sendMail(options);
  }
}



