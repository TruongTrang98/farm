const axios = require('axios');

class UploadImageService {
  async upload(req) {
    // console.log("FILE >>>>>>>", req);
    try {
      const result = await axios(
        {
          method: 'POST',
          url: 'http://localhost:3001/image/upload',
          req
        }
      )
      console.log("RESULT", result.data);
      return true;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = UploadImageService;