const CategoryModel = require('../models/CategoryModel');

const changePositionOfProductId = async (oldCategoryId, newCategoryId, productId) => {
  try {
    const oldCategory = await CategoryModel.findOne({
      isDeleted: false,
      isActive: true,
      _id: oldCategoryId
    })
    oldCategory.products = oldCategory.products.filter(i => i !== productId);
    await CategoryModel.updateOne({
      _id: oldCategory
    }, {
      $set: {
        products: oldCategory.products
      }
    })

    await CategoryModel.updateOne({
      _id: newCategoryId
    }, {
      $push: {
        products: productId
      }
    })
  } catch (error) {
    throw error;
  }
}

module.exports = changePositionOfProductId;