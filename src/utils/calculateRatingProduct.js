const CommentModel = require('../models/CommentModel');
const BlogModel = require('../models/BlogModel');
const ProductModel = require('../models/ProductModel');

const calculateRating = async (productId, blogId) => {
  try {
    const commentsOfProduct = await CommentModel.find({
      isDeleted: false,
      isActive: true,
      productId,
      blogId
    })
    let rating = commentsOfProduct.reduce((acc, cur) => {
      return acc + cur.rating
    }, 0)
    rating = rating / commentsOfProduct.length;
    if (productId) {
      await ProductModel.updateOne(
        {
          _id: productId
        },
        {
          $set: {
            rating
          }
        }
      )
    }
    if (blogId) {
      await BlogModel.updateOne(
        {
          _id: blogId
        },
        {
          $set: {
            rating
          }
        }
      )
    }
  } catch (error) {
    throw error;
  }
}

module.exports = calculateRating;