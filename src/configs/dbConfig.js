const mongoose = require('mongoose');
const USER_DB = process.env.USER_DB || null;
const USER_PASSWORD = process.env.USER_PASSWORD || null;
// const URi = `mongodb+srv://${USER_DB}:${USER_PASSWORD}@kadonfarm.9b06p.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`
const URi = "mongodb://127.0.0.1:27017"

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
  autoIndex: false, // Don't build indexes
  poolSize: 10, // Maintain up to 10 socket connections
  serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
  family: 4 // Use IPv4, skip trying IPv6
};

mongoose
  .connect(URi, options)
  .then(
    () => console.log("Database connection successfully"),
    error => {
      console.log("Can not connect database. Error", error)
      process.exit(1)
    }
  )
  .catch(error => {
    console.log(error)
    process.exit(1)
  });

module.exports = mongoose;