const ProductModel = require('../models/ProductModel');
const CategoryModel = require('../models/CategoryModel');
const CommentModel = require('../models/CommentModel');
const RemoveAccents = require('../helpers/removeAccents');
const StatusCode = require('../constants/StatusCode');
const changePositionOfProductId = require('../utils/changePositionProductId');
const validObjectId = require('../helpers/validObjectId');
const imagePath = require('../configs/urlConfig');

module.exports = {
  async search(req, res) {
    try {
      const { payload } = req.body;
      const objCondition = {
        isDeleted: false
      }
      const objSort = { createdAt: -1 };
      let offset = 0;
      let limit = 100000;
      if (!payload) {
        return res.status(StatusCode.INVALID_PARAMS).json({
          message: "Thông tin đầu vào không hợp lệ"
        })
      }
      if (payload) {
        if (payload.filter) {
          const { filter } = payload;
          if (filter.id) {
            validObjectId.check(res, filter.id);
            objCondition["_id"] = filter.id;
          }

          if (filter.keyword) {
            const regex = new RegExp(filter.keyword, 'gi');
            objCondition["$or"] = [
              {
                "code": regex
              },
              {
                "name": regex
              }
            ]
          }
        }

        if (payload.paging) {
          if (payload.paging.offset) {
            offset = payload.paging.offset;
          }
          if (payload.paging.limit) {
            limit = payload.paging.limit;
          }
        }
      }
      // Search Product
      console.log(objCondition);
      let products = await ProductModel.find(objCondition)
        .skip(offset * limit)
        .sort(objSort)
        .limit(limit);

      // Search comment 
      if (payload && payload.filter && payload.filter.id) {
        const productIds = products.map(i => i._id);
        let comments = await CommentModel.find({
          isDeleted: false,
          isActive: true,
          productId: payload.filter.id
        })
        if (comments && comments.length > 0) {
          const objComment = {};
          for (const commentItem of comments) {
            if (!objComment[commentItem.productId]) {
              objComment[commentItem.productId] = [commentItem]
            } else {
              objComment[commentItem.productId].push(commentItem);
            }
          }
          // comments = comments.reduce((acc, cur) => {
          //   acc["comments"]
          //   return {
          //     ...acc,
          //     [cur.productId]: comment
          //   }
          // }, {})
          products = JSON.parse(JSON.stringify(products));
          products.forEach(i => {
            i["comments"] = objComment[i._id];
          })
        }
      }

      const total = await ProductModel.count(objCondition);
      return res.status(StatusCode.REQUEST_SUCCESS).json({
        total,
        items: products
      });
    } catch (error) {
      throw error;
    }
  },

  async create(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số truyền vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { code, name, image, price, categoryId, description, isHighLight } = payload;
    if (!code || !name || !image || !price || !categoryId) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    validObjectId.check(res, categoryId);
    try {
      const checkExisted = await ProductModel.findOne({
        code: code,
        isDeleted: false
      })
      if (checkExisted) {
        result.message = "Mã sản phẩm đã tồn tại"
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const existedCategory = await CategoryModel.findOne({
        isDeleted: false,
        isActive: true,
        _id: categoryId
      });
      if (!existedCategory) {
        result.message = "Mã danh mục không tồn tại";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const unsignedName = RemoveAccents.removeVietnameseTones(name);
      const created = await ProductModel.create({
        code,
        name,
        unsignedName,
        description,
        image,
        price,
        isHighLight,
        categoryId
      })
      if (!created) {
        result.message = "Không thể tạo mới sản phẩm!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      // const newProducts = [...new Set([...existedCategory.products, created._id])];
      // await CategoryModel.updateOne({
      //   _id: existedCategory._id
      // }, {
      //   $set: {
      //     products: newProducts
      //   }
      // }) 
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  // async create(req, res) {
  //   const result = {
  //     "message": ""
  //   }
  //   const { code, name, price, categoryId, description } = req.body;
  //   const file = req.file;
  //   result.message = "Tham số truyền vào không hợp lệ";
  //   // if (!payload) {
  //   //   return res.status(StatusCode.INVALID_PARAMS).json(result);
  //   // }
  //   if (!code || !name || !file || !price || !categoryId) {
  //     return res.status(StatusCode.INVALID_PARAMS).json(result);
  //   }
  //   if (!file) {
  //     return res.status(401).json({ error: 'Vui lòng cung cấp ảnh!' });
  //   }
  //   validObjectId.check(res, categoryId);
  //   try {
  //     /*********** Resolve upload image **************/
  //     const image = `${imagePath.domainUrl}/${file.filename}`;
  //     /***********************************************/
  //     const checkExisted = await ProductModel.findOne({
  //       code: code,
  //       isDeleted: false
  //     })
  //     if (checkExisted) {
  //       result.message = "Mã sản phẩm đã tồn tại"
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     const existedCategory = await CategoryModel.findOne({
  //       isDeleted: false,
  //       isActive: true,
  //       _id: categoryId
  //     });
  //     if (!existedCategory) {
  //       result.message = "Mã danh mục không tồn tại";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     const unsignedName = RemoveAccents.removeVietnameseTones(name);
  //     const created = await ProductModel.create({
  //       code,
  //       name,
  //       unsignedName,
  //       description,
  //       image,
  //       price,
  //       categoryId
  //     })
  //     if (!created) {
  //       result.message = "Không thể tạo mới sản phẩm!!!";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     result.message = "Tạo thành công";
  //     result.code = 200;
  //     // const newProducts = [...new Set([...existedCategory.products, created._id])];
  //     // await CategoryModel.updateOne({
  //     //   _id: existedCategory._id
  //     // }, {
  //     //   $set: {
  //     //     products: newProducts
  //     //   }
  //     // }) 
  //     return res.status(StatusCode.REQUEST_SUCCESS).json(result);
  //   } catch (error) {
  //     throw error;
  //   }
  // },

  async delete(req, res) {
    const result = {
      "message": ""
    }
    try {
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedProduct = await ProductModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedProduct) {
        result.message = "Không tìm thấy thông tin sản phẩm";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      // const category = await CategoryModel.findOne({
      //   isDeleted: false,
      //   isActive: true,
      //   _id: existedProduct.categoryId
      // })
      // if (!category) {
      //   result.message = "Không tìm thấy thông tin danh mục";
      //   return res.status(StatusCode.REQUEST_FAIL).json(result);
      // }
      const deleted = await ProductModel.updateOne({
        _id: id
      }, {
        isDeleted: true
      })
      if (!deleted || deleted.ok !== 1) {
        result.message = "Không thể xóa sản phẩm này";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Xóa thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async update(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const { payload } = req.body;
      let { name, price, categoryId, image } = payload;
      if (!payload) {
        result.message = "Thông tin đầu vào không hợp lệ";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      // const { name, price, categoryId } = req.body;
      // const file = req.file;
      let existedProduct = await ProductModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedProduct) {
        result.message = "Không tìm thấy thông tin sản phẩm";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      existedProduct = JSON.parse(JSON.stringify(existedProduct));
      delete existedProduct._id;
      delete existedProduct.createdAt;
      delete existedProduct.updatedAt;
      delete existedProduct.code;
      let updateData = {
        name, price, categoryId, image
      };
      for (const x of Object.keys(existedProduct)) {
        // if (x === "image") {
        //   updateData["image"] = file ? `${imagePath.domainUrl}/${file.filename}` : existedProduct[x];
        //   continue;
        // }
        updateData[x] = payload[x] || existedProduct[x]
      }
      updateData["unsignedName"] = RemoveAccents.removeVietnameseTones(updateData["name"])
      const updated = await ProductModel.updateOne(
        { _id: id },
        {
          $set: updateData
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      if (categoryId !== existedProduct.categoryId) {
        await changePositionOfProductId(existedProduct.categoryId, categoryId, id);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async updateActive(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedProduct = await ProductModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedProduct) {
        result.message = "Không tìm thấy thông tin sản phẩm";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const updated = await ProductModel.updateOne(
        { _id: id },
        {
          $set: {
            isActive: !existedProduct.isActive
          }
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async updateHighLight(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedProduct = await ProductModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedProduct) {
        result.message = "Không tìm thấy thông tin sản phẩm";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const updated = await ProductModel.updateOne(
        { _id: id },
        {
          $set: {
            isHighLight: !existedProduct.isHighLight
          }
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}