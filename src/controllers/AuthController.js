const AccountModel = require('../models/AccountModel');
const jwtHelper = require("../helpers/jwt.helper");
const StatusCode = require('../constants/StatusCode');
const accessTokenLife = process.env.ACCESS_TOKEN_LIFE || "1h";
const accessTokenSecret = process.env.SECRET_KEY || "access-token-secret-kadon-farm@";
const refreshTokenLife = process.env.REFRESH_TOKEN_LIFE || "3650d";
const refreshTokenSecret = process.env.REFRESH_TOKEN_SECRET || "refresh-token-secret-kadon-farm@";

module.exports = {
  async login(req, res) {
    try {
      const result = {
        message: ""
      }

      const { username, password } = req.body;
      const existedAccount = await AccountModel.findOne({
        isDeleted: false,
        isActive: false,
        username,
        password,
      })

      if (!existedAccount) {
        result.message = "Không tìm thấy thông tin tài khoản";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }

      const userDate = {
        _id: existedAccount.id,
        username: existedAccount.username
      }

      const accessToken = await jwtHelper.generateToken(userData, accessTokenSecret, accessTokenLife);
      const refreshToken = await jwtHelper.generateToken(userData, refreshTokenSecret, refreshTokenLife);
      result.message = "Thành công";
      result.accessToken = accessToken;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}