const BlogModel = require('../models/BlogModel');
const CommentModel = require('../models/CommentModel');
const RemoveAccents = require('../helpers/removeAccents');
const StatusCode = require('../constants/StatusCode');
const validObjectId = require('../helpers/validObjectId');
const imagePath = require('../configs/urlConfig');

module.exports = {
  async search(req, res) {
    const { payload } = req.body;
    const objCondition = {
      isDeleted: false,
      isBlog: true
    }
    const objSort = { createdAt: -1 };
    let offset = 0;
    let limit = 100000;
    if (payload) {
      if (payload.filter) {
        const { filter } = payload;
        if (filter.id) {
          validObjectId.check(res, filter.id);
          objCondition["_id"] = filter.id;
        }
        if (filter.keyword) {
          const regex = RegExp(filter.keyword, 'gi');
          objCondition["$or"] = [
            {
              title: regex
            },
            {
              unsignedTitle: regex
            }
          ]
        }
      }
      if (payload.paging) {
        if (payload.paging.offset) {
          offset = payload.paging.offset;
        }
        if (payload.paging.limit) {
          limit = payload.paging.limit;
        }
      }
    }
    console.log(objCondition);
    const blogs = await BlogModel.find(objCondition)
      .skip(offset * limit)
      .sort(objSort)
      .limit(limit);
    // Search comment 
    if (payload && payload.filter && payload.filter.id) {
      const blogIds = blogs.map(i => i._id);
      let comments = await CommentModel.find({
        isDeleted: false,
        isActive: true,
        blogId: {
          $in: blogIds
        }
      })
      if (comments && comments.length > 0) {
        const objComment = {};
        for (const commentItem of comments) {
          if (!objComment[commentItem.blogId]) {
            objComment[commentItem.blogId] = [commentItem]
          } else {
            objComment[commentItem.blogId].push(commentItem);
          }
        }
        blogs = JSON.parse(JSON.stringify(blogs));
        blogs.forEach(i => {
          i["comments"] = objComment[i._id];
        })
      }
      await BlogModel.updateOne(
        {
          _id: payload.filter.id
        },
        {
          $set: {
            count: blogs[0].count + 1
          }
        }
      )
    }
    const total = await BlogModel.count(objCondition)
    return res.status(StatusCode.REQUEST_SUCCESS).json({
      total,
      items: blogs
    });
  },

  async create(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số truyền vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { title, image, content, isBlog } = payload;
    // const file = req.file;
    if (!title || !content || !image) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    // if (!file) {
    //   return res.status(401).json({ error: 'Vui lòng cung cấp ảnh!' });
    // }
    try {
      /*********** Resolve upload image **************/
      // const image = `${imagePath.domainUrl}/${file.filename}`;
      /***********************************************/
      const unsignedTitle = RemoveAccents.removeVietnameseTones(title);
      const created = await BlogModel.create({
        title,
        unsignedTitle,
        image,
        isBlog,
        content
      })
      if (!created) {
        result.message = "Không thể tạo mới bài viết!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async delete(req, res) {
    const result = {
      "message": ""
    }
    try {
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedBlog = await BlogModel.findOne({
        isDeleted: false,
        isBlog: true,
        _id: id
      })
      if (!existedBlog) {
        result.message = "Không tìm thấy thông tin bài viết";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const deleted = await BlogModel.updateOne({
        _id: id
      }, {
        isDeleted: true
      })
      if (!deleted || deleted.ok !== 1) {
        result.message = "Không thể xóa bài viết này";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Xóa thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async update(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const { payload } = req.body;
      if (!payload) {
        result.message = "Thông tin đầu vào không hợp lệ";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      const { title, image, content } = payload;
      // const { title, content } = req.body
      // const file = req.file;
      let existedBlog = await BlogModel.findOne({
        isDeleted: false,
        isBlog: true,
        _id: id
      })
      if (!existedBlog) {
        result.message = "Không tìm thấy thông tin bài viết";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      existedBlog = JSON.parse(JSON.stringify(existedBlog));
      delete existedBlog._id;
      delete existedBlog.createdAt;
      delete existedBlog.updatedAt;
      delete existedBlog.code;
      let updateData = {
        title, content, image
      };
      for (const x of Object.keys(existedBlog)) {
        // if (x === 'image') {
        //   updateData["image"] = file ? `${imagePath.domainUrl}/${file.filename}` : existedBlog[x];
        //   continue;
        // }
        updateData[x] = payload[x] || existedBlog[x]
      }
      updateData["unsignedTitle"] = RemoveAccents.removeVietnameseTones(updateData["title"])
      const updated = await BlogModel.updateOne(
        { _id: id },
        {
          $set: updateData
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async updateActive(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedBlog = await BlogModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedBlog) {
        result.message = "Không tìm thấy thông tin bài viết";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const updated = await BlogModel.updateOne(
        { _id: id },
        {
          $set: {
            isActive: !existedBlog.isActive
          }
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}