const Resize = require('../helpers/resize');
const imagePath = require('../configs/urlConfig');

module.exports = {
  async uploadImage(req, res) {
    // const fileUpload = new Resize('./src/public/images');
    const file = req.file;
    if (!file) {
      res.status(401).json({ error: 'Vui lòng cung cấp ảnh!' });
    }
    // const filename = await fileUpload.save(req.file.buffer);
    return res.status(200).json({ url: `${imagePath.domainUrl}/${file.filename}` });
  }
}