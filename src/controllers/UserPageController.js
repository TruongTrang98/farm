const BlogModel = require('../models/BlogModel');
const ProductModel = require('../models/ProductModel');
const PromotionModel = require('../models/PromotionModel');
const CommentModel = require('../models/CommentModel');
const CarouselModel = require('../models/CarouselModel');
const CategoryModel = require('../models/CategoryModel');
const UserMailModel = require('../models/UserMailModel');
const ContactInfoModel = require('../models/ContactInfoModel');
const OrderModel = require('../models/OrderModel');
const StatusCode = require('../constants/StatusCode');
const validateEmail = require('../helpers/validateEmail');
const SortProductType = require('../constants/SortProduct');
const validObjectId = require('../helpers/validObjectId');
const OrderStatus = require('../constants/OrderStatus');
const PhoneHelper = require('../helpers/phoneHelper');
const calculateRating = require('../utils/calculateRatingProduct');

module.exports = {
  async searchHomePage(req, res) {
    const blogs = await BlogModel.find({
      isDeleted: false,
      isActive: true,
      isBlog: true
    })
      .sort({ createdAt: -1 })
      .limit(6);
    const carousels = await CarouselModel.find({
      isDeleted: false,
      isActive: true
    })
    const promotions = await PromotionModel.find({
      isDeleted: false,
      isActive: true
    })
      .sort({ updatedAt: -1 })
      .limit(6);
    // if(promotions) {
    //   for(const promotionItem of promotions) {
    //     if(pro)
    //   }
    // }
    const categories = await CategoryModel.find({
      isDeleted: false,
      isActive: true
    })
      .sort({ updatedAt: -1 })

    const productsHighLight = await ProductModel.find({
      isDeleted: false,
      isActive: true,
      "$or": [
        {
          isHighLight: true
        },
        {
          count: {
            $gt: 1
          }
        }
      ]
    })
      .sort({ count: -1, updatedAt: -1 })
      .limit(8);

    const bestSellProduct = await ProductModel.find({
      isDeleted: false,
      isActive: true
    }).sort({ sellCount: -1 }).limit(8);

    const newProducts = await ProductModel.find({
      isDeleted: false,
      isActive: true
    }).sort({ updatedAt: -1 }).limit(8)

    const responseData = {
      blogs,
      promotions,
      carousels,
      categories,
      bestSellProduct,
      productsHighLight,
      newProducts
    }
    return res.status(StatusCode.REQUEST_SUCCESS).json({
      responseData
    });
  },

  async searchProductPage(req, res) {
    try {
      const responseData = {
        total: undefined,
        items: undefined
      }
      let { payload } = req.body;
      let objSort = {};
      let offset, limit;
      let productId = undefined;
      const objCondition = {
        isDeleted: false,
        isActive: true
      }
      if (payload) {
        if (payload.filter) {
          const { filter } = payload;
          let { categoryId, fromPrice, toPrice, keyword, sortProductType, rating } = filter;
          productId = filter.productId;
          if (categoryId) {
            validObjectId.check(res, categoryId);
            objCondition["categoryId"] = "" + categoryId;
          }
          if (productId) {
            validObjectId.check(res, productId);
            objCondition["_id"] = "" + productId;
          }
          if (keyword) {
            const regex = new RegExp(keyword, 'gi');
            objCondition["$or"] = [
              {
                "name": {
                  $regex: regex
                }
              }
            ]
          }

          if (sortProductType) {
            switch (sortProductType) {
              case SortProductType.POPULATE: {
                objSort = {
                  count: -1
                }
                break;
              }
              case SortProductType.HIGH_RATING: {
                objSort = {
                  rating: -1
                }
                break;
              }
              case SortProductType.NEWEST: {
                objSort = {
                  createdAt: -1
                }
                break;
              }
              case SortProductType.LOW_TO_HIGH: {
                objSort = {
                  price: 1
                }
                break;
              }
              case SortProductType.HIGH_TO_LOW: {
                objSort = {
                  price: -1
                }
                break;
              }
            }
          }

          if (rating) {
            objCondition["rating"] = {
              $gte: rating
            }
          }

          fromPrice = fromPrice || 0;
          toPrice = toPrice || 1000000000;

          objCondition["$and"] = [
            {
              price: {
                $gte: fromPrice
              }
            },
            {
              price: {
                $lte: toPrice
              }
            }
          ]
        }

        if (payload.paging) {
          offset = payload.paging.offset || 0;
          limit = payload.paging.limit || 100000000;
        }
      }
      console.log(objCondition);
      const products = await ProductModel.find(objCondition)
        .populate({
          path: 'categoryId',
          select: 'id code name'
        })
        .skip(offset * limit)
        .limit(limit)
        .sort(objSort);

      let comments = undefined;
      let relativeProducts = undefined;
      if (products && products.length > 0) {
        if (productId) {
          comments = await CommentModel.find({
            isDeleted: false,
            isActive: true,
            productId: productId
          })
            .sort({ createdAt: -1 })
          products[0] = JSON.parse(JSON.stringify(products[0]));
          products[0].comments = comments || [];
          relativeProducts = await ProductModel.find({
            isDeleted: false,
            isActive: true,
            categoryId: products[0].categoryId,
            _id: {
              $ne: productId
            }
          })
          await ProductModel.updateOne(
            {
              _id: productId
            },
            {
              $set: {
                count: products[0].count + 1
              }
            }
          )
          responseData.relativeProducts = relativeProducts;
        }
      }
      const total = await ProductModel.count(objCondition)
      responseData.total = total;
      responseData.items = products;
      return res.status(StatusCode.REQUEST_SUCCESS).json(responseData);
    } catch (error) {
      throw error;
    }
  },

  async searchBlogPage(req, res) {
    const result = {
      total: undefined,
      items: undefined
    }
    try {
      let { payload } = req.body;
      const objCondition = {
        isDeleted: false,
        isActive: true,
        isBlog: true
      }
      let offset = 0;
      let limit = 100000;
      if (payload) {
        if (payload.filter) {
          const { id, keyword } = payload.filter;
          if (id) {
            objCondition["_id"] = id;
          }
          if (keyword) {
            const regex = new RegExp(keyword, 'gi');
            objCondition["$or"] = [
              {
                "title": {
                  $regex: regex
                }
              },
              {
                "unsignedTitle": {
                  $regex: regex
                }
              }
            ]
          }
        }
        if (payload.paging) {
          offset = payload.paging.offset;
          limit = payload.paging.limit;
        }
      }
      console.log({ objCondition });
      const blogs = await BlogModel.find(objCondition)
        .skip(offset * limit)
        .limit(limit);
      let comments = undefined;
      let newBlog = await BlogModel.find({
        isDeleted: false,
        isActive: true,
        isBlog: true
      }).sort({ updatedAt: -1 });
      result.newestBlog = newBlog;
      if (blogs && blogs.length > 0) {
        if (payload && payload.filter && payload.filter.id) {
          const { id } = payload.filter
          comments = await CommentModel.find({
            isDeleted: false,
            isActive: true,
            blogId: id
          })
            .sort({ createdAt: -1 })
          blogs[0] = JSON.parse(JSON.stringify(blogs[0]));
          blogs[0].comments = comments || [];
        }
      }
      const total = await BlogModel.count(objCondition)
      result.items = blogs;
      result.total = total;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async searchCharityPage(req, res) {
    const result = {
      total: undefined,
      items: undefined
    }
    try {
      let { payload } = req.body;
      const objCondition = {
        isDeleted: false,
        isActive: true,
        isBlog: false
      }
      let offset = 0;
      let limit = 100000;
      if (payload) {
        if (payload.filter) {
          const { id, keyword } = payload.filter;
          if (id) {
            objCondition["_id"] = id;
          }
          if (keyword) {
            const regex = new RegExp(keyword, 'gi');
            objCondition["$or"] = [
              {
                "title": {
                  $regex: regex
                }
              },
              {
                "unsignedTitle": {
                  $regex: regex
                }
              }
            ]
          }
        }
        if (payload.paging) {
          offset = payload.paging.offset;
          limit = payload.paging.limit;
        }
      }
      console.log({ objCondition });
      const blogs = await BlogModel.find(objCondition)
        .skip(offset * limit)
        .limit(limit);
      let newBlog = await BlogModel.find({
        isDeleted: false,
        isActive: true,
        isBlog: false
      }).sort({ updatedAt: -1 });
      result.newestBlog = newBlog;
      if (blogs && blogs.length > 0) {
        if (payload && payload.filter && payload.filter.id) {
          const { id } = payload.filter
          comments = await CommentModel.find({
            isDeleted: false,
            isActive: true,
            blogId: id
          })
            .sort({ createdAt: -1 })

          blogs[0] = JSON.parse(JSON.stringify(blogs[0]));
          blogs[0].comments = comments || [];
        }
      }
      const total = await BlogModel.count(objCondition)
      result.items = blogs;
      result.total = total;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async searchCategory(req, res) {
    try {
      const categories = await CategoryModel.find({
        isDeleted: false,
        isActive: true
      })

      return res.status(StatusCode.REQUEST_SUCCESS).json(categories);
    } catch (error) {
      throw error;
    }
  },

  async createOrder(req, res) {
    const result = {
      "message": ""
    }

    try {
      const { payload } = req.body;
      result.message = "Tham số truyền vào không hợp lệ";
      if (!payload) {
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      console.log("PAYLOAD >>>>>>", payload);
      let { name, gender, phone, email, address, orderDetail, promotionCode } = payload;
      if (!name || !gender || !phone || !email || !address) {
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      if (!validateEmail.validateEmail(email)) {
        result.message = "Email không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      if (!(new PhoneHelper).validatePhone(phone)) {
        result.message = "Số điện thoại không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      if (!orderDetail || orderDetail.length <= 0) {
        result.message = "Thông tin đơn hàng không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      if (promotionCode) {
        const checkedPromotionCode = await PromotionModel.findOne({
          code: promotionCode,
          isDeleted: false,
          isActive: true
        })

        if (!checkedPromotionCode) {
          result.message = "Thông tin mã giảm giá không hợp lệ. Vui lòng kiểm tra lại!";
          return res.status(StatusCode.INVALID_PARAMS).json(result);
        }
      }

      let subTotal = 0, total = 0;
      const productIds = orderDetail.map(i => {
        console.log(i);
        validObjectId.check(res, i.productId)
        return i.productId;
      });
      const checkedProduct = await ProductModel.find({
        isDeleted: false,
        isActive: true,
        _id: {
          $in: productIds
        }
      })

      if (!checkedProduct || checkedProduct.length <= 0) {
        result.message = "Thông tin đơn hàng không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      } else {
        if (checkedProduct.length !== productIds.length) {
          result.message = "Thông tin đơn hàng không hợp lệ. Vui lòng kiểm tra lại!";
          return res.status(StatusCode.INVALID_PARAMS).json(result);
        }
      }
      orderDetail.forEach(order => {
        if (order.amount <= 0) {
          result.message = "Thông tin đơn hàng không hợp lệ. Vui lòng kiểm tra lại!";
          return res.status(StatusCode.INVALID_PARAMS).json(result);
        }
        subTotal += order.amount * order.price;
      })
      const objProduct = checkedProduct.reduce((acc, cur) => {
        return {
          ...acc,
          [cur._id]: cur
        }
      }, {})

      orderDetail = orderDetail.map(item => ({
        product: {
          id: item.productId,
          code: objProduct[item.productId].code,
          name: objProduct[item.productId].name
        },
        amount: item.amount,
        price: item.price
      }))

      let totalRemission = 0;
      /********* Gỉai quyết promotion here ************/
      /************************************************/
      total = subTotal - (totalRemission || 0);
      const objCreate = {
        name,
        gender,
        email,
        phone,
        address,
        orderDetail,
        subtotal: subTotal || 0,
        totalRemission: totalRemission || 0,
        total,
        status: OrderStatus.PENDING
      }
      console.log(objCreate);
      const created = await OrderModel.create(objCreate)
      if (!created) {
        result.message = "Không thể tạo mới bài viết!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async registerNotification(req, res) {
    try {
      const result = {
        message: ""
      }
      const { payload } = req.body;
      if (!payload) {
        return res.status(StatusCode.INVALID_PARAMS).json({
          message: "Thông tin đầu vào không hợp lệ!"
        })
      }
      const { email } = payload;
      if (!validateEmail.validateEmail(email)) {
        result.message = "Email không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      const created = await UserMailModel.create({
        email
      })
      if (!created) {
        result.message = "Không thể  thêm mới email!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async createContactInfo(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số truyền vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { name, email, phone, address, content } = payload;
    if (!name || !email || !phone || !address || !content) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    try {
      const created = await ContactInfoModel.create({
        name, email, phone, address, content
      })
      if (!created) {
        result.message = "Không thể tạo mới thông tin liên hệ!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async createComment(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số truyền vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { name, email, phone, content, productId, blogId, rating } = payload;
    if (!name || !email || !phone || !content) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    validObjectId.check(res, productId || blogId);
    if (productId) {
      console.log("PRODUCT");
      const checked = await ProductModel.find({
        isDeleted: false,
        _id: productId
      })

      if (!checked || checked.length <= 0) {
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
    }

    if (blogId) {
      console.log("BLOG");
      const checked = await BlogModel.find({
        isDeleted: false,
        _id: blogId
      })

      if (!checked || checked.length <= 0) {
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
    }
    try {
      const created = await CommentModel.create({
        name,
        email,
        phone,
        content,
        rating,
        productId,
        blogId
      })
      if (!created) {
        result.message = "Không thể tạo mới đánh giá!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      if (productId) {
        await calculateRating(productId);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}