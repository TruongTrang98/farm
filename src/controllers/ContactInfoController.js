const ContactInfoModel = require('../models/ContactInfoModel');
const RemoveAccents = require('../helpers/removeAccents');
const StatusCode = require('../constants/StatusCode');
const validObjectId = require('../helpers/validObjectId');

module.exports = {
  async search(req, res) {
    const { payload } = req.body;
    const objCondition = {
      isDeleted: false
    }
    const objSort = { createdAt: -1 };
    let offset = 0;
    let limit = 100000;
    if (payload) {
      if (payload.filter) {
        const { filter } = payload;
        if (filter.id) {
          validObjectId.check(res, filter.id);
          objCondition["_id"] = filter.id;
        }

        if (filter.keyword) {
          const regex = RegExp(filter.keyword, 'gi');
          objCondition["$or"] = [
            {
              name: regex
            },
            {
              email: regex
            },
            {
              phone: regex
            }
          ]
        }
      }
      if (payload.paging) {
        if (payload.paging.offset) {
          offset = payload.paging.offset;
        }
        if (payload.paging.limit) {
          limit = payload.paging.limit;
        }
      }
    }
    console.log({ objCondition });
    const contactInfos = await ContactInfoModel.find(objCondition)
      .skip(offset * limit)
      .sort(objSort)
      .limit(limit);
    const total = await ContactInfoModel.count(objCondition)
    return res.status(StatusCode.REQUEST_SUCCESS).json({
      total,
      items: contactInfos
    });
  },

  async create(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số truyền vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { name, email, phone, address, content } = params;
    if (!name || !email || !phone || !address || !content) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    try {
      const created = await ContactInfoModel.create({
        name, email, phone, address, content
      })
      if (!created) {
        result.message = "Không thể tạo mới thông tin liên hệ!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async delete(req, res) {
    const result = {
      "message": ""
    }
    try {
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedContactInfo = await ContactInfoModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedContactInfo) {
        result.message = "Không tìm thấy thông tin";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const deleted = await ContactInfoModel.updateOne({
        _id: id
      }, {
        isDeleted: true
      })
      if (!deleted || deleted.ok !== 1) {
        result.message = "Không thể xóa bài viết này";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Xóa thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  //#region 
  // async update(req, res) {
  //   try {
  //     const result = {
  //       "message": ""
  //     }
  //     const id = req.params.id;
  //     const { title, image, content } = req.body;
  //     let existedContactInfo = await ContactInfoModel.findOne({
  //       isDeleted: false,
  //       _id: id
  //     })
  //     if (!existedContactInfo) {
  //       result.message = "Không tìm thấy thông tin";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     existedContactInfo = JSON.parse(JSON.stringify(existedContactInfo));
  //     delete existedContactInfo._id;
  //     delete existedContactInfo.createdAt;
  //     delete existedContactInfo.updatedAt;
  //     delete existedContactInfo.code;
  //     let updateData = {
  //       title, image, content
  //     };
  //     for (const x of Object.keys(existedContactInfo)) {
  //       updateData[x] = req.body.params[x] || existedContactInfo[x]
  //     }
  //     updateData["unsignedTitle"] = RemoveAccents.removeVietnameseTones(updateData["title"])
  //     const updated = await ContactInfoModel.updateOne(
  //       { _id: id },
  //       {
  //         $set: updateData
  //       }
  //     )
  //     if (!updated && updated.nModified !== 1) {
  //       result.message = "Không thể cập nhật";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     result.message = "Cập nhật thành công";
  //     return res.status(StatusCode.REQUEST_SUCCESS).json(result);
  //   } catch (error) {
  //     throw error;
  //   }
  // },

  // async updateActive(req, res) {
  //   try {
  //     const result = {
  //       "message": ""
  //     }
  //     const id = req.params.id;
  //     const existedContactInfo = await ContactInfoModel.findOne({
  //       isDeleted: false,
  //       _id: id
  //     })
  //     if (!existedContactInfo) {
  //       result.message = "Không tìm thấy thông tin bài viết";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     const updated = await ContactInfoModel.updateOne(
  //       { _id: id },
  //       {
  //         $set: {
  //           isActive: !existedContactInfo.isActive
  //         }
  //       }
  //     )
  //     if (!updated && updated.nModified !== 1) {
  //       result.message = "Không thể cập nhật";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     result.message = "Cập nhật thành công";
  //     return res.status(StatusCode.REQUEST_SUCCESS).json(result);
  //   } catch (error) {
  //     throw error;
  //   }
  // }
  //#endregion
}