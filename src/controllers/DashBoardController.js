const BlogController = require('../controllers/BlogController');
const CategoryModel = require('../models/CategoryModel');
const ProductModel = require('../models/ProductModel');
const OrderModel = require('../models/OrderModel');
const OrderStatus = require('../constants/OrderStatus');
const StatusCode = require('../constants/StatusCode');

module.exports = {
  async getStatisticsDashboard = (req, res) => {
    const totalBlog = await BlogController.countDocuments({
      isDeleted: false,
      isActive: true,
      isBlog: true
    })

    const totalCharity = await BlogController.countDocuments({
      isDeleted: false,
      isActive: true,
      isBlog: false
    })

    const totalCategory = await CategoryModel.countDocuments({
      isDeleted: false,
      isActive: true
    })

    const totalProduct = await ProductModel.countDocuments({
      isDeleted: false,
      isActive: true
    })

    const totalOrderPending = await OrderModel.countDocuments({
      status: OrderStatus.PENDING,
      isDeleted: false
    })

    const totalOrderReceived = await OrderModel.countDocuments({
      status: OrderStatus.RECEIVED,
      isDeleted: false
    })

    const totalOrderDone = await OrderModel.countDocuments({
      status: OrderStatus.DONE,
      isDeleted: false
    })

    // Tổng tiền
    const totalMoney = await OrderModel.aggregate([
      {
        "$match": {
          status: "DONE"
        }
      },
      {
        $group: {
          _id: null,
          "total": {
            $sum: "$total"
          }
        }
      }
    ])

    console.log(totalMoney);

    // Tổng đơn hàng theo tháng
    //

    return res.status(StatusCode.REQUEST_SUCCESS).json({
      totalCategory,
      totalBlog,
      totalCharity,
      totalProduct,
      totalOrderPending,
      totalOrderReceived,
      totalOrderDone
    })
  }
}