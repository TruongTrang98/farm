const CommentModel = require('../models/CommentModel');
const ProductModel = require('../models/ProductModel');
const BlogModel = require('../models/BlogModel');
const RemoveAccents = require('../helpers/removeAccents');
const StatusCode = require('../constants/StatusCode');
const calculateRating = require('../utils/calculateRatingProduct');
const validObjectId = require('../helpers/validObjectId');

module.exports = {
  async search(req, res) {
    const { payload } = req.body;
    const objCondition = {
      isDeleted: false
    }
    const objSort = { createdAt: -1 };
    let offset = 0;
    let limit = 100000;
    if (payload) {
      if (payload.filter) {
        const { filter } = payload;
        if (filter.isBlog) {
          objCondition["productId"] = null;
        }
        if (filter.id) {
          validObjectId.check(res, filter.id);
          objCondition["_id"] = filter.id;
        }
        if (filter.productId) {
          validObjectId.check(res, filter.productId);
          objCondition["productId"] = filter.productId;
        }
        if (filter.blogId) {
          validObjectId.check(res, filter.blogId);
          objCondition["blogId"] = filter.blogId;
        }
        if (filter.keyword) {
          const regex = RegExp(filter.keyword, 'gi');
          objCondition["$or"] = [
            {
              name: regex
            },
            {
              email: regex
            },
            {
              phone: regex
            },
            {
              content: regex
            }
          ]
        }
      }
      if (payload.paging) {
        if (payload.paging.offset) {
          offset = payload.paging.offset;
        }
        if (payload.paging.limit) {
          limit = payload.paging.limit;
        }
      }
    }
    console.log(objCondition);
    console.log(limit, offset);
    const comments = await CommentModel.find(objCondition)
      .skip(offset * limit)
      .limit(limit)
      .sort(objSort)
    const total = await CommentModel.count(objCondition)
    return res.status(StatusCode.REQUEST_SUCCESS).json({
      total,
      items: comments
    });
  },

  async create(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số truyền vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { name, email, phone, content, rating, productId, blogId } = payload;
    if (!name || !email || !phone || !content || !rating) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    validObjectId.check(res, productId || blogId);
    if (productId) {
      console.log("PRODUCT");
      const checked = await ProductModel.find({
        isDeleted: false,
        _id: productId
      })

      if (!checked || checked.length <= 0) {
        result.message = "Không tìm thấy thông tin sản phẩm";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
    }

    if (blogId) {
      console.log("BLOG");
      const checked = await BlogModel.find({
        isDeleted: false,
        _id: blogId
      })

      if (!checked || checked.length <= 0) {
        result.message = "Không tìm thấy thông tin bài viết";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
    }
    try {
      const created = await CommentModel.create({
        name,
        email,
        phone,
        content,
        rating,
        productId,
        blogId
      })
      if (!created) {
        result.message = "Không thể tạo mới đánh giá!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      await calculateRating(productId, blogId);
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async delete(req, res) {
    console.log("DELETE COMMENT API");
    const result = {
      "message": ""
    }
    try {
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedComment = await CommentModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedComment) {
        result.message = "Không tìm thấy thông tin bình luận";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      await calculateRating(existedComment.productId, existedComment.blogId);
      const deleted = await CommentModel.updateOne({
        _id: id
      }, {
        isDeleted: true
      })
      if (!deleted || deleted.ok !== 1) {
        result.message = "Không thể xóa";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Xóa thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async update(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const { payload } = req.body;
      if (!payload) {
        result.message = "Tham số truyền vào không hợp lệ";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      const { name, email, phone, content, rating, productId, blogId } = payload;
      let existedComment = await CommentModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedComment) {
        result.message = "Không tìm thấy thông tin bình luận";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      existedComment = JSON.parse(JSON.stringify(existedComment));
      delete existedComment._id;
      delete existedComment.createdAt;
      delete existedComment.updatedAt;
      delete existedComment.code;
      let updateData = {
        name, email, phone, content, rating, productId, blogId
      };
      for (const x of Object.keys(existedComment)) {
        updateData[x] = req.body.payload[x] || existedComment[x]
      }
      const updated = await CommentModel.updateOne(
        { _id: id },
        {
          $set: updateData
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      await calculateRating(productId, blogId);
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async updateActive(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedComment = await CommentModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedComment) {
        result.message = "Không tìm thấy thông tin bình luận";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const updated = await CommentModel.updateOne(
        { _id: id },
        {
          $set: {
            isActive: !existedComment.isActive
          }
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      await calculateRating(existedComment.productId || existedComment.blogId);
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}