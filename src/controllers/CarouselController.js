const CarouselModel = require('../models/CarouselModel');
const validObjectId = require('../helpers/validObjectId');
const imagePath = require('../configs/urlConfig');

module.exports = {
  async search(req, res) {
    const { payload } = req.body;
    const objCondition = {
      isDeleted: false
    }
    const objSort = { createdAt: -1 };
    let offset = 0;
    let limit = 100000;
    if (payload) {
      if (payload.filter) {
        const { filter } = payload;
        if (filter.id) {
          validObjectId.check(res, filter.id);
          objCondition["_id"] = filter.id;
        }
        if (filter.keyword) {
          const regex = RegExp(filter.keyword, 'gi');
          objCondition["$or"] = [
            {
              title: regex
            }
          ]
        }
      }
      if (payload.paging) {
        if (payload.paging.offset) {
          offset = payload.paging.offset;
        }
        if (payload.paging.limit) {
          limit = payload.paging.limit;
        }
      }
    }
    console.log(objCondition);
    const blogs = await CarouselModel.find(objCondition)
      .skip(offset * limit)
      .sort(objSort)
      .limit(limit);
    const total = await CarouselModel.count(objCondition)
    return res.status(StatusCode.REQUEST_SUCCESS).json({
      total,
      items: blogs
    });
  },

  async create(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số truyền vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { title, image } = payload;
    if (!title || !image) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    try {
      const created = await CarouselModel.create({
        title,
        image
      })
      if (!created) {
        result.message = "Không thể up hình carousel mới!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async delete(req, res) {
    const result = {
      "message": ""
    }
    try {
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedCarousel = await CarouselModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedCarousel) {
        result.message = "Không tìm thấy carousel";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const deleted = await CarouselModel.updateOne({
        _id: id
      }, {
        isDeleted: true
      })
      if (!deleted || deleted.ok !== 1) {
        result.message = "Không thể xóa carousel";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Xóa thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async update(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const { payload } = req.body;
      if (!payload) {
        result.message = "Thông tin đầu vào không hợp lệ";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      const { title, image } = payload;
      // const { title, content } = req.body
      // const file = req.file;
      let existedCarousel = await CarouselModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedCarousel) {
        result.message = "Không tìm thấy thông tin bài viết";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      existedCarousel = JSON.parse(JSON.stringify(existedCarousel));
      delete existedCarousel._id;
      delete existedCarousel.createdAt;
      delete existedCarousel.updatedAt;
      let updateData = {
        title, image
      };
      for (const x of Object.keys(existedCarousel)) {
        updateData[x] = payload[x] || existedCarousel[x]
      }
      const updated = await CarouselModel.updateOne(
        { _id: id },
        {
          $set: updateData
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async updateActive(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedCarousel = await CarouselModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedCarousel) {
        result.message = "Không tìm thấy carousel";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const updated = await CarouselModel.updateOne(
        { _id: id },
        {
          $set: {
            isActive: !existedCarousel.isActive
          }
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}