const CategoryModel = require('../models/CategoryModel');
const ProductModel = require('../models/ProductModel');
const RemoveAccents = require('../helpers/removeAccents');
const StatusCode = require('../constants/StatusCode');
const validObjectId = require('../helpers/validObjectId');
const imagePath = require('../configs/urlConfig');

module.exports = {
  async search(req, res) {
    const { payload } = req.body;
    const objCondition = {
      isDeleted: false
    }
    let offset = 0;
    let limit = 100000;
    if (payload) {
      if (payload.filter) {
        const { filter } = payload;
        if (filter.id) {
          validObjectId.check(res, filter.id);
          objCondition["_id"] = filter.id
        }
        if (filter.keyword) {
          const regex = RegExp(filter.keyword, 'gi');
          objCondition["$or"] = [
            {
              code: regex
            },
            {
              name: regex
            }
          ]
        }
        if (payload.paging) {
          if (payload.paging.offset) {
            offset = payload.paging.offset;
          }
          if (payload.paging.limit) {
            limit = payload.paging.limit;
          }
        }
      }
    }
    console.log(objCondition)
    const categories = await CategoryModel.find(objCondition)
      .populate('products')
      .skip(offset * limit)
      .limit(limit);
    const total = await CategoryModel.count(objCondition)
    return res.status(StatusCode.REQUEST_SUCCESS).json({
      total,
      items: categories
    });
  },

  async create(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số đầu vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { code, name, image } = payload;
    if (!code || !name || !image) {
      return res.status(StatusCode.INVALID_payload).json(result);
    }

    try {
      const checkExisted = await CategoryModel.findOne({
        code: code,
        isDeleted: false
      })

      if (checkExisted) {
        result.message = "Mã danh mục đã tồn tại!!!"
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const unsignedName = RemoveAccents.removeVietnameseTones(name);
      const created = await CategoryModel.create({
        code,
        name,
        image,
        unsignedName
      })
      if (!created) {
        result.message = "Không thể tạo mới danh mục!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  // async create(req, res) {
  //   const result = {
  //     "message": ""
  //   }
  //   // const { payload } = req.body;
  //   result.message = "Tham số đầu vào không hợp lệ";
  //   // if (!payload) {
  //   //   return res.status(StatusCode.INVALID_PARAMS).json(result);
  //   // }
  //   const { code, name } = req.body;
  //   const file = req.file;
  //   if (!code || !name) {
  //     return res.status(StatusCode.INVALID_payload).json(result);
  //   }
  //   if (!file) {
  //     return res.status(401).json({ error: 'Vui lòng cung cấp ảnh!' });
  //   }
  //   try {
  //     const image = `${imagePath.domainUrl}/${file.filename}`;
  //     const checkExisted = await CategoryModel.findOne({
  //       code: code,
  //       isDeleted: false
  //     })

  //     if (checkExisted) {
  //       result.message = "Mã danh mục đã tồn tại!!!"
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     const unsignedName = RemoveAccents.removeVietnameseTones(name);
  //     const created = await CategoryModel.create({
  //       code,
  //       name,
  //       unsignedName,
  //       image,
  //     })
  //     if (!created) {
  //       result.message = "Không thể tạo mới danh mục!!!";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     result.message = "Tạo thành công";
  //     result.code = 200;
  //     return res.status(StatusCode.REQUEST_SUCCESS).json(result);
  //   } catch (error) {
  //     throw error;
  //   }
  // },

  async delete(req, res) {
    const result = {
      "message": ""
    }
    try {
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedCategory = await CategoryModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedCategory) {
        result.message = "Không tìm thấy thông tin danh mục";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const productsOfCategory = await ProductModel.find({
        isDeleted: false,
        categoryId: existedCategory._id
      })
      if (productsOfCategory.length > 0) {
        result.message = "Không thể xóa danh mục. Danh mục đang chứa sản phẩm!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const deleted = await CategoryModel.updateOne({
        _id: id
      }, {
        isDeleted: true
      })
      if (!deleted || deleted.ok !== 1) {
        result.message = "Không thể xóa danh mục này";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Xóa thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async update(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      let { payload } = req.body;
      let { name, image } = payload;
      // let file = req.file;
      const existedCategory = await CategoryModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedCategory) {
        result.message = "Không tìm thấy thông tin danh mục";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      name = name || existedCategory.name;
      image = image || existedCategory.image;
      // image = file ? `${imagePath.domainUrl}/${file.filename}` : existedCategory.image;
      const updated = await CategoryModel.updateOne(
        { _id: id },
        {
          $set: {
            name,
            image
          }
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async updateActive(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedCategory = await CategoryModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedCategory) {
        result.message = "Không tìm thấy thông tin danh mục";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const updated = await CategoryModel.updateOne(
        { _id: id },
        {
          $set: {
            isActive: !existedCategory.isActive
          }
        }
      )

      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}