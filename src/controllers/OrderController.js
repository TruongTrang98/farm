const OrderModel = require('../models/OrderModel');
const ProductModel = require('../models/ProductModel');
const PromotionModel = require('../models/PromotionModel');
const RemoveAccents = require('../helpers/removeAccents');
const StatusCode = require('../constants/StatusCode');
const OrderStatus = require('../constants/OrderStatus');
const validObjectId = require('../helpers/validObjectId');
const validateEmail = require('../helpers/validateEmail');
const PhoneHelper = require('../helpers/phoneHelper');

module.exports = {
  async search(req, res) {
    const { payload } = req.body;
    const objCondition = {
      isDeleted: false
    }
    const objSort = { createdAt: -1 };
    let offset = 0;
    let limit = 100000;
    if (payload) {
      const { filter } = payload;
      if (filter) {
        if (filter.id) {
          objCondition["_id"] = filter.id;
        }
        if (filter.keyword) {
          const regex = RegExp(filter.keyword, 'gi');
          objCondition["$or"] = [
            {
              "name": regex
            },
            {
              "email": regex
            },
            {
              "phone": regex
            }
          ]
        }
      }
      if (payload.paging) {
        if (payload.paging.offset) {
          offset = payload.paging.offset;
        }
        if (payload.paging.limit) {
          limit = payload.paging.limit;
        }
      }
    }
    console.log({ objCondition });
    const orders = await OrderModel.find(objCondition)
      .populate({
        path: 'orderDetail.product.id',
        select: '_id image'
      })
      .skip(offset * limit)
      .sort(objSort)
      .limit(limit);
    console.log(orders[0]);
    const total = await OrderModel.count(objCondition)
    return res.status(StatusCode.REQUEST_SUCCESS).json({
      total,
      items: orders
    });
  },

  async create(req, res) {
    const result = {
      "message": ""
    }

    try {
      const { payload } = req.body;
      result.message = "Tham số truyền vào không hợp lệ";
      if (!payload) {
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      console.log("PAYLOAD >>>>>>", payload);
      let { name, gender, phone, email, orderDetail, promotionCode } = payload;
      if (!name || !gender || !phone || !email) {
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      if (!validateEmail.validateEmail(email)) {
        result.message = "Email không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      if (!(new PhoneHelper).validatePhone(phone)) {
        result.message = "Số điện thoại không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      if (!orderDetail || orderDetail.length <= 0) {
        result.message = "Thông tin đơn hàng không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      }
      if (promotionCode) {
        const checkedPromotionCode = await PromotionModel.findOne({
          code: promotionCode,
          isDeleted: false,
          isActive: true
        })

        if (!checkedPromotionCode) {
          result.message = "Thông tin mã giảm giá không hợp lệ. Vui lòng kiểm tra lại!";
          return res.status(StatusCode.INVALID_PARAMS).json(result);
        }
      }

      let subTotal = 0, total = 0;
      const productIds = orderDetail.map(i => {
        console.log(i);
        validObjectId.check(res, i.productId)
        return i.productId;
      });
      const checkedProduct = await ProductModel.find({
        isDeleted: false,
        isActive: true,
        _id: {
          $in: productIds
        }
      })

      if (!checkedProduct || checkedProduct.length <= 0) {
        result.message = "Thông tin đơn hàng không hợp lệ. Vui lòng kiểm tra lại!";
        return res.status(StatusCode.INVALID_PARAMS).json(result);
      } else {
        if (checkedProduct.length !== productIds.length) {
          result.message = "Thông tin đơn hàng không hợp lệ. Vui lòng kiểm tra lại!";
          return res.status(StatusCode.INVALID_PARAMS).json(result);
        }
      }
      orderDetail.forEach(order => {
        if (order.amount <= 0) {
          result.message = "Thông tin đơn hàng không hợp lệ. Vui lòng kiểm tra lại!";
          return res.status(StatusCode.INVALID_PARAMS).json(result);
        }
        subTotal += order.amount * order.price;
      })
      const objProduct = checkedProduct.reduce((acc, cur) => {
        return {
          ...acc,
          [cur._id]: cur
        }
      }, {})

      orderDetail = orderDetail.map(item => ({
        product: {
          id: item.productId,
          code: objProduct[item.productId].code,
          name: objProduct[item.productId].name
        },
        amount: item.amount,
        price: item.price
      }))

      let totalRemission = 0;
      /********* Gỉai quyết promotion here ************/
      /************************************************/
      total = subTotal - (totalRemission || 0);
      const objCreate = {
        name,
        gender,
        email,
        phone,
        orderDetail,
        subtotal: subTotal || 0,
        totalRemission: totalRemission || 0,
        total,
        status: OrderStatus.PENDING
      }
      console.log(objCreate);
      const created = await OrderModel.create(objCreate)
      if (!created) {
        result.message = "Không thể tạo mới bài viết!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async delete(req, res) {
    console.log("DELETE ORDER");
    const result = {
      "message": ""
    }
    try {
      const id = req.params.id;
      const existedOrder = await OrderModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedOrder) {
        result.message = "Không tìm thấy thông tin đơn hàng";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const deleted = await OrderModel.updateOne({
        _id: id
      }, {
        isDeleted: true
      })
      if (!deleted || deleted.ok !== 1) {
        result.message = "Không thể xóa đơn hàng này";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Xóa thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  // async update(req, res) {
  //   try {
  //     const result = {
  //       "message": ""
  //     }
  //     const id = req.params.id;
  //     const { name, gender, email, phone } = req.body;
  //     let existedOrder = await OrderModel.findOne({
  //       isDeleted: false,
  //       _id: id
  //     })
  //     if (!existedOrder) {
  //       result.message = "Không tìm thấy thông tin đơn hàng";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     existedOrder = JSON.parse(JSON.stringify(existedOrder));
  //     delete existedOrder._id;
  //     delete existedOrder.createdAt;
  //     delete existedOrder.updatedAt;
  //     delete existedOrder.code;
  //     let updateData = {
  //       title, image, content
  //     };
  //     for (const x of Object.keys(existedOrder)) {
  //       updateData[x] = req.body.params[x] || existedOrder[x]
  //     }
  //     updateData["unsignedTitle"] = RemoveAccents.removeVietnameseTones(updateData["title"])
  //     const updated = await OrderModel.updateOne(
  //       { _id: id },
  //       {
  //         $set: updateData
  //       }
  //     )
  //     if (!updated && updated.nModified !== 1) {
  //       result.message = "Không thể cập nhật";
  //       return res.status(StatusCode.REQUEST_FAIL).json(result);
  //     }
  //     result.message = "Cập nhật thành công";
  //     return res.status(StatusCode.REQUEST_SUCCESS).json(result);
  //   } catch (error) {
  //     throw error;
  //   }
  // },

  async updateStatus(req, res) {
    try {
      const result = {
        "message": ""
      }
      const objStatus = {
        [OrderStatus.CANCEL]: [OrderStatus.PENDING],
        [OrderStatus.PENDING]: [OrderStatus.RECEIVED, OrderStatus.CANCEL],
        [OrderStatus.RECEIVED]: [OrderStatus.CANCEL, OrderStatus.DONE],
        [OrderStatus.DONE]: [OrderStatus.CANCEL]
      }
      const id = req.params.id;
      validObjectId.check(res, id);
      const { payload } = req.body;
      if (!payload) {
        console.log("Không có payload");
        result.message = "Thông tin đầu vào không hợp lệ. (Không tìm thấy payload)";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const { status } = payload;
      if (!status) {
        console.log("Không có status");
        result.message = "Không thấy thông tin trạng thái (Không thấy payload.status)";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      if (!OrderStatus[status]) {
        result.message = "Trạng thái đơn hàng không hợp lệ (Không có trạng thái trong hệ thống)";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const existedOrder = await OrderModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedOrder) {
        result.message = "Không tìm thấy thông tin đơn hàng";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      if (!objStatus[existedOrder.status].includes(status)) {
        result.message = `Trạng thái đơn hàng không hợp lệ. Trạng thái đơn hàng hiện tại ${existedOrder.status}, trạng thái mới là ${status}`;
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      // if (!objStatus[existedOrder.includes(status)) {
      //   result.message = "Trạng thái mới không hợp lệ. Vui lòng kiểm tra lại!";
      //   return res.status(StatusCode.REQUEST_FAIL).json(result);
      // }
      const updated = await OrderModel.updateOne(
        { _id: id },
        {
          $set: {
            status,
          }
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật trạng thái đơn hàng";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      if (status === OrderStatus.DONE) {
        let ids = existedOrder.orderDetail.map(item => item.product.id);
        await ProductModel.updateMany(
          {
            _id: {
              $in: ids
            }
          },
          {
            $inc: {
              sellCount: 1
            }
          }
        )
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}