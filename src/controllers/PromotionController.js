const PromotionModel = require('../models/PromotionModel');
const RemoveAccents = require('../helpers/removeAccents');
const StatusCode = require('../constants/StatusCode');
const validObjectId = require('../helpers/validObjectId');

module.exports = {
  async search(req, res) {
    const { payload } = req.body;
    const objCondition = {
      isDeleted: false
    }
    const objSort = { createdAt: -1 };
    let offset = 0;
    let limit = 100000;
    if (payload) {
      if (payload.filter) {
        const { filter } = payload;
        if (filter.id) {
          validObjectId.check(res, filter.id);
          objCondition["_id"] = filter.id;
        }
        if (filter.keyword) {
          const regex = RegExp(filter.keyword, 'gi');
          objCondition["$or"] = [
            {
              code: regex
            },
            {
              name: regex
            }
          ]
        }
      }
      if (payload.paging) {
        if (payload.paging.offset) {
          offset = payload.paging.offset;
        }
        if (payload.paging.limit) {
          limit = payload.paging.limit;
        }
      }
    }
    const promotions = await PromotionModel.find(objCondition)
      .skip(offset * limit)
      .sort(objSort)
      .limit(limit);
    return res.status(StatusCode.REQUEST_SUCCESS).json({
      total: promotions.length,
      items: promotions
    });
  },

  async create(req, res) {
    const result = {
      "message": ""
    }
    const { payload } = req.body;
    result.message = "Tham số truyền vào không hợp lệ";
    if (!payload) {
      return res.status(StatusCode.INVALID_PARAMS).json(result);
    }
    const { code, name, approvedWithProduct, value, type, amount, startDate, expiredDate } = payload;
    try {
      const checkExisted = await PromotionModel.findOne({
        code: code,
        isDeleted: false
      })

      if (checkExisted) {
        result.message = "Mã  khuyếh mãi đã tồn tại!!!"
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const unsignedName = RemoveAccents.removeVietnameseTones(name);
      const created = await PromotionModel.create({
        code,
        name,
        approvedWith,
        unsignedName,
        image,
        type,
        value,
        amount,
        startDate,
        expiredDate
      })
      if (!created) {
        result.message = "Không thể tạo mới chương trình khuyến mãi!!!";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Tạo thành công";
      result.code = 200;
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async delete(req, res) {
    const result = {
      "message": ""
    }
    try {
      const id = req.params.id;
      validObjectId.check(res, id);
      const existedPromotion = await PromotionModel.findOne({
        isDeleted: false,
        _id: id,
      })
      if (!existedPromotion) {
        result.message = "Không tìm thấy thông tin chương trình khuyến mãi";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const deleted = await PromotionModel.updateOne({
        isDeleted: false,
        _id: id
      }, {
        isDeleted: true
      })
      if (!deleted || deleted.ok !== 1) {
        result.message = "Không thể xóa bài viết này";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Xóa thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async update(req, res) {
    try {
      const result = {
        "message": ""
      }
      const code = req.params.code;
      const { name, approvedWithProduct, type, value, amount, startDate, expiredDate } = req.body;
      let existedPromotion = await PromotionModel.findOne({
        isDeleted: false,
        code,
      })
      if (!existedPromotion) {
        result.message = "Không tìm thấy thông tin chương trình khuyến mãi";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      existedPromotion = JSON.parse(JSON.stringify(existedPromotion));
      delete existedPromotion._id;
      delete existedPromotion.createdAt;
      delete existedPromotion.updatedAt;
      delete existedPromotion.code;
      let updateData = {
        name, approvedWithProduct, type, value, amount, startDate, expiredDate
      };
      for (const x of Object.keys(existedPromotion)) {
        updateData[x] = req.body.params[x] || existedPromotion[x]
      }
      updateData["unsignedName"] = RemoveAccents.removeVietnameseTones(updateData["name"])
      const updated = await PromotionModel.updateOne(
        { code },
        {
          $set: updateData
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  },

  async updateActive(req, res) {
    try {
      const result = {
        "message": ""
      }
      const id = req.params.id;
      const existedPromotion = await PromotionModel.findOne({
        isDeleted: false,
        _id: id
      })
      if (!existedPromotion) {
        result.message = "Không tìm thấy thông tin chương trình khuyến mãi";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      const updated = await PromotionModel.updateOne(
        { _id: id },
        {
          $set: {
            isActive: !existedPromotion.isActive
          }
        }
      )
      if (!updated && updated.nModified !== 1) {
        result.message = "Không thể cập nhật";
        return res.status(StatusCode.REQUEST_FAIL).json(result);
      }
      result.message = "Cập nhật thành công";
      return res.status(StatusCode.REQUEST_SUCCESS).json(result);
    } catch (error) {
      throw error;
    }
  }
}