const OrderStatus = {
  'PENDING': 'PENDING',
  'RECEIVED': 'RECEIVED',
  'DONE': 'DONE',
  'CANCEL': 'CANCEL'
}

OrderStatus.getText = (value) => {
  switch (value) {
    case OrderStatus.PENDING: {
      return 'Chờ nhận';
    }
    case OrderStatus.RECEIVED: {
      return 'Đã nhận đơn hàng';
    }
    case OrderStatus.DONE: {
      return 'Đã hoàn thành';
    }
    case OrderStatus.CANCEL: {
      return 'Đã hủy'
    }
  }
}

module.exports = OrderStatus