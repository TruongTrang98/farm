const PromotionType = {
  'PERCENT': 'PERCENT',
  'VND': 'VND',
  'OTHER': 'OTHER'
}

PromotionType.getText = (value) => {
  switch (value) {
    case PromotionType.PERCENT: {
      return 'Giảm phần trăm';
    }
    case PromotionType.RECEIVED: {
      return 'Giảm tiền mặt';
    }
    case PromotionType.OTHER: {
      return 'Khác'
    }
  }
}

module.exports = PromotionType;