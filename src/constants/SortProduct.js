const SortProductType = {
  POPULATE: 'POPULATE',
  HIGH_RATING: 'HIGH_RATING',
  NEWEST: 'NEWEST',
  LOW_TO_HIGH: 'LOW_TO_HIGH',
  HIGH_TO_LOW: 'HIGH_TO_LOW'
}

SortProductType.getText = (value) => {
  switch (value) {
    case SortProductType.POPULATE: {
      return 'Mức độ phổ biến';
    }
    case SortProductType.HIGH_RATING: {
      return 'Đánh giá cao';
    }
    case SortProductType.NEWEST: {
      return 'Mới nhất';
    }
    case SortProductType.LOW_TO_HIGH: {
      return 'Giá thấp tới cao';
    }
    case SortProductType.HIGH_TO_LOW: {
      return 'Giá cao tới thấp';
    }
  }
}

module.exports = SortProductType;