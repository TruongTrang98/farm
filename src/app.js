const express = require('express');
const PORT = process.env.PORT || 8080;
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const route = require('./routes/index');
const userRoute = require('./routes/user.route');
const publicDir = require('path').join(__dirname, '/public');
const UploadService = require('./services/UploadImageServices');
const multerHelper = require('./helpers/multerHelper');

/** config some middlewares **/
app.use(cors());
app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
/***********/
app.use(express.static(publicDir))
app.use('/api', route);
app.use('/user', userRoute);

// app.use(fileUpload());
// app.post("/upload", async (req, res) => {
//   console.log("TEST UPLOAD IMAGE SERVICE");
//   console.log(req);
//   const upload = new UploadService();
//   await upload.upload(req);
//   return res.send("OK");
// })

app.listen(PORT, () => {
  console.log(`Server is running at port ${PORT}`);
})