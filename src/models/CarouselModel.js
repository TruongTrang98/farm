const mongoose = require('mongoose');
const { Schema } = mongoose;

const carouselSchema = new Schema({
  // numberOrder: {
  //   type: Number,
  //   default: 1
  // },
  title: {
    type: String,
    required: true
  },
  link: {
    type: String,
    required: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('Carousel', carouselSchema);