const mongoose = require('mongoose');
const { Schema } = mongoose;

const commentSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    required: true,
    trim: true
  },
  phone: {
    type: String,
    required: true,
    trim: true
  },
  content: {
    type: String,
    required: true
  },
  rating: {
    type: Number,
    default: 5
  },
  productId: {
    type: Schema.Types.ObjectId,
    ref: 'Product'
  },
  blogId: {
    type: Schema.Types.ObjectId,
    ref: 'Blog'
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('Comment', commentSchema);