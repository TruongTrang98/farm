const mongoose = require('mongoose');
const { Schema } = mongoose;

const userEmailSchema = new Schema({
  email: {
    type: String,
    required: true,
    trim: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('UserMail', userEmailSchema);