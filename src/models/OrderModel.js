const mongoose = require('mongoose');
const { Schema } = mongoose;
const gender = require('../constants/Gender');
const OrderStatus = require('../constants/OrderStatus');

const orderSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  gender: {
    type: String,
    required: true,
    enum: Object.keys(gender)
  },
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  orderDetail: [
    {
      _id: false,
      product: {
        id: {
          type: Schema.Types.ObjectId,
          ref: 'Product',
          required: true
        },
        code: {
          type: String,
          required: true
        },
        name: {
          type: String,
          required: true
        }
      },
      amount: {
        type: Number,
        required: true,
        default: 1
      },
      price: {
        type: Number,
        required: true
      }
    }
  ],
  subtotal: {
    type: Number,
    default: 0
  },
  promotionCode: {
    type: Schema.Types.ObjectId,
    ref: 'Promotion',
    default: null
  },
  totalRemission: {
    type: Number,
    default: 0
  },
  total: {
    type: Number,
    default: 0
  },
  status: {
    type: String,
    enum: Object.keys(OrderStatus),
    default: OrderStatus.PENDING
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('Order', orderSchema);