const mongoose = require('mongoose');
const { Schema } = mongoose;
const PromotionType = require('../constants/PromotionType');

const promotionSchema = new Schema({
  code: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  unsignedName: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  description: {
    type: String
  },
  approvedWithProduct: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
  // approveWithCategory: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
  type: {
    enum: Object.keys(PromotionType),
    // required: true
  },
  value: {
    type: Number,
    // required: true,
    default: 0
  },
  amount: {
    type: Number,
    default: 0
  },
  remaining: {
    type: Number,
    default: 0
  },
  startDate: {
    type: Date,
    default: new Date()
  },
  expiredDate: {
    type: Date,
    // required: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('Promotion', promotionSchema);