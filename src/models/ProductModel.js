const mongoose = require('mongoose');
const { Schema } = mongoose;

const productSchema = new Schema({
  code: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  unsignedName: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  image: {
    type: String
  },
  description: {
    type: String,
    default: null
  },
  price: {
    type: Number,
    required: true,
    default: 0
  },
  rating: {
    type: Number,
    default: 0
  },
  count: {
    type: Number,
    default: 0
  },
  sellCount: {
    type: Number,
    default: 0
  },
  categoryId: {
    type: Schema.Types.ObjectId,
    ref: 'Category'
  },
  isHighLight: {
    type: Boolean,
    default: false,
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('Product', productSchema);