const mongoose = require('mongoose');
const { Schema } = mongoose;

const categoriesSchema = new Schema({
  code: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  unsignedName: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  image: {
    type: String,
    unique: true,
    required: true,
  },
  products: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
  isDeleted: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('Category', categoriesSchema);