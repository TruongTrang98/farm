const uploadRoute = require('express').Router();
const multer = require('multer');
const UploadImageController = require('../controllers/UploadController');
// const upload = require('../middleware/UploadMiddleware');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './src/public/images');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

uploadRoute.post('/upload', multer({ storage }).single('img'), UploadImageController.uploadImage);

module.exports = uploadRoute;