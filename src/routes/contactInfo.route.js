const contactInfoRoute = require('express').Router();
const ContactInfoController = require('../controllers/ContactInfoController');

contactInfoRoute.post('/', ContactInfoController.search)
contactInfoRoute.post('/create', ContactInfoController.create);
contactInfoRoute.delete('/delete/:id', ContactInfoController.delete);
// contactInfoRoute.put('/update/:id', ContactInfoController.update);
// contactInfoRoute.put('/updateActive/:id', ContactInfoController.updateActive);

module.exports = contactInfoRoute;