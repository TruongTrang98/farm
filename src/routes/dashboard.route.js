const blogRoute = require('express').Router();
const DashBoardController = require('../controllers/DashBoardController');

blogRoute.get('/', DashBoardController.getStatisticsDashboard)

module.exports = blogRoute;