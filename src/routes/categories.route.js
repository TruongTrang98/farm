const categoryRoute = require('express').Router();
const CategoryController = require('../controllers/CategoryController');
const multerHelper = require('../helpers/multerHelper');

categoryRoute.post('/', CategoryController.search)
categoryRoute.post('/create', multerHelper.single('img'), CategoryController.create);
categoryRoute.delete('/delete/:id', CategoryController.delete);
categoryRoute.put('/update/:id', multerHelper.single('img'), CategoryController.update);
categoryRoute.put('/updateActive/:id', CategoryController.updateActive);

module.exports = categoryRoute;