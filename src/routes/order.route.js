const orderRoute = require('express').Router();
const OrderController = require('../controllers/OrderController');

orderRoute.post('/', OrderController.search)
orderRoute.post('/create', OrderController.create);
orderRoute.delete('/delete/:id', OrderController.delete);
orderRoute.put('/updateStatus/:id', OrderController.updateStatus);
// orderRoute.put('/update/:id', OrderController.update);
// orderRoute.put('/updateHighLight/:id', OrderController.updateHighLight);

module.exports = orderRoute;