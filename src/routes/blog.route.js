const blogRoute = require('express').Router();
const BlogController = require('../controllers/BlogController');
const multerHelper = require('../helpers/multerHelper');

blogRoute.post('/', BlogController.search)
blogRoute.post('/create', multerHelper.single('img'), BlogController.create);
blogRoute.delete('/delete/:id', BlogController.delete);
blogRoute.put('/update/:id', multerHelper.single('img'), BlogController.update);
blogRoute.put('/updateActive/:id', BlogController.updateActive);

module.exports = blogRoute;