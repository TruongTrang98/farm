const carouselRoute = require('express').Router();
const CarouselController = require('../controllers/CarouselController');

carouselRoute.post('/', CarouselController.search)
carouselRoute.post('/create', multerHelper.single('img'), CarouselController.create);
carouselRoute.delete('/delete/:id', CarouselController.delete);
carouselRoute.put('/update/:id', multerHelper.single('img'), CarouselController.update);
carouselRoute.put('/updateActive/:id', CarouselController.updateActive);

module.exports = carouselRoute;