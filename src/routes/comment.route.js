const commentRoute = require('express').Router();
const CommentController = require('../controllers/CommentController');

commentRoute.post('/', CommentController.search)
commentRoute.post('/create', CommentController.create);
commentRoute.delete('/delete/:id', CommentController.delete);
commentRoute.put('/update/:id', CommentController.update);
commentRoute.put('/updateActive/:id', CommentController.updateActive);

module.exports = commentRoute;