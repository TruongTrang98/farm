const promotionRoute = require('express').Router();
const PromotionController = require('../controllers/PromotionController');

promotionRoute.post('/', PromotionController.search)
promotionRoute.post('/create', PromotionController.create);
promotionRoute.delete('/delete/:code', PromotionController.delete);
promotionRoute.put('/update/:code', PromotionController.update);
promotionRoute.put('/updateActive/:code', PromotionController.updateActive);

module.exports = promotionRoute;