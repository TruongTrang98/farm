userRoute = require('express').Router();
const UserPageController = require('../controllers/UserPageController');

userRoute.get('/', UserPageController.searchHomePage);
userRoute.post('/product', UserPageController.searchProductPage);
userRoute.post('/blog', UserPageController.searchBlogPage);
userRoute.post('/charity', UserPageController.searchCharityPage);
userRoute.post('/category', UserPageController.searchCategory);
userRoute.post('/order', UserPageController.createOrder);
userRoute.post('/comment', UserPageController.createComment);
userRoute.post('/registerNotification', UserPageController.registerNotification);
userRoute.post('/createContactInfo', UserPageController.createContactInfo);

module.exports = userRoute;