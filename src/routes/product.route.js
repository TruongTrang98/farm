const productRoute = require('express').Router();
const ProductController = require('../controllers/ProductController');
const multerHelper = require('../helpers/multerHelper');

productRoute.post('/', ProductController.search)
productRoute.post('/create', multerHelper.single('img'), ProductController.create);
productRoute.delete('/delete/:id', ProductController.delete);
productRoute.put('/update/:id', multerHelper.single('img'), ProductController.update);
productRoute.put('/updateActive/:id', ProductController.updateActive);
productRoute.put('/updateHighLight/:id', ProductController.updateHighLight);

module.exports = productRoute;