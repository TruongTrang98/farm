const router = require('express').Router();
const categoryRoute = require('./categories.route');
const productRoute = require('./product.route');
const commentRoute = require('./comment.route');
const orderRoute = require('./order.route');
const promotionRoute = require('./promotion.route');
const contactInfoRoute = require('./contactInfo.route');
const blogRoute = require('./blog.route');
const uploadRouter = require('./upload.route');
const Authentication = require('../middleware/AuthMiddleware');

router.use('/images', uploadRouter);

// router.use(Authentication.isAuth)
router.use("/category", categoryRoute);
router.use("/product", productRoute);
router.use('/blog', blogRoute);
router.use('/comment', commentRoute);
router.use('/order', orderRoute);
router.use('/promotion', promotionRoute);
router.use('/contactInfo', contactInfoRoute);


module.exports = router;