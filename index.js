require('dotenv').config();
(async function () {
  try {
    await require('./src/configs/dbConfig');
    require('./src/app');
  } catch (error) {
    console.log("Error. Cannot connect database. Please check again!!!");
    throw error;
  }
})()